# Synchronize Cameras
- Define your rtsp streams in `cameras.json`
- press `<space>` to start / stop recording
- press `<q>` to quit.
- with `analyze_video.py` you can have a look, if produced videos are well enough synchronized. 