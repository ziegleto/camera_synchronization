import cv2
import numpy as np

class Capture(cv2.VideoCapture):
    def __init__(self, path: str, name: str) -> None:
        super().__init__(path)
        self.name = name
        self.frame_history = []

    def save_frame(self, frame):
        self.frame_history.append(frame)


class FrameCompariser:
    def __init__(self) -> None:
        self.frames_per_captures = {}
        self.nr_of_saved_frames = 0

    def run(self, captures: list[Capture], frame_number: int) -> None:
        assert FrameCompariser.check_total_frames(captures, frame_number)
        FrameCompariser.show_captures(captures, frame_number)

    @staticmethod
    def get_max_frames(captures): 
        return min(
            [cap.get(cv2.CAP_PROP_FRAME_COUNT) for cap in captures]
        )

    @staticmethod
    def show_captures(captures: list[Capture], frame_number: int) -> None:
        for cap in captures:
            cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number) 
            ret, frame = cap.read()
            if ret:
                cap.save_frame(frame)
                cv2.imshow(cap.name, frame)
        cv2.waitKey(0)

    @staticmethod
    def check_total_frames(captures: list[Capture], frame_number: int) -> bool:
        total_frames = FrameCompariser.get_max_frames(captures)
        return frame_number > 0 and frame_number <= total_frames


if __name__ == '__main__':
    SAMPLE_SIZE = 10
    c1 = Capture('out/analyze_delay/FRONTAL.avi', 'FRONTAL')
    c2 = Capture('out/analyze_delay/LEFT.avi', 'LEFT')
    caps = [c1, c2]

    fc = FrameCompariser()
    max_frame = int(fc.get_max_frames(caps))

    for frame_nr in range(max_frame):
        print(frame_nr+1)
        fc.run(caps, frame_nr+1)

    